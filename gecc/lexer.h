
#pragma once

// Create enum from .def file
typedef enum KeywordType
{
    #define KEYWORD_DEFINITION(x) KEYWORD_##x,
    #include "keywords.def"
    #undef KEYWORD_DEFINITION

    KEYWORD_COUNT,
    KEYWORD_NOTHING = -1,
} KeywordType;

// Create enum from .def file
typedef enum SymbolType
{
    #define SYMBOL_DEFINITION(n,x) SYMBOL_##n,
    #include "symbols.def"
    #undef SYMBOL_DEFINITION

    SYMBOL_COUNT,
    SYMBOL_NOTHING = -1
};

void LexerInit();
void LexerTokenize();