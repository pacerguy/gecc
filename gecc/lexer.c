
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "lexer.h"

enum ReadState
{
    RS_NOTHING,
    RS_COMMENT,
    RS_COMMENTLINE,
    RS_TEXT,
    RS_NUMBER,
    RS_SYMBOLS,
    RS_STRING,
    RS_PREPROCESSOR
};

char *gKeywords[] =
{
    #define KEYWORD_DEFINITION(x) #x,
    #include "keywords.def"
    #undef KEYWORD_DEFINITION
};

char *gSymbols[] =
{
    #define SYMBOL_DEFINITION(n, x) #x,
    #include "symbols.def"
    #undef SYMBOL_DEFINITION
};

struct LexerState
{
    int currentLine;
    int currentColumn;

    enum ReadState readState;

    int currentTokenLength;
    int currentTokenMaximum;
    int currentTokenColumn;
    char *currentToken;

    int stringEscape;
    int octalEscape;
    int octalEscapeValue;
    int hexEscape;
    int hexEscapeValue;
};
struct LexerState gLexerState;

// Internal functions
void _LexerReset();
void _LexerStoreCharacter(char newChar);
void _LexerFlushToken();
int _LexerWhiteSpace(char newChar);
int _LexerText(char newChar);
int _LexerNumber(char newChar);
int _LexerSymbol(char newChar);
int _LexerComment(char newChar);
int _LexerCommentLine(char newChar);
int _LexerString(char newChar);
int _LexerPreprocessor(char newChar);
enum KeywordType _LexerGetKeywordType(char *text);



void LexerInit()
{
    gLexerState.currentTokenMaximum = 1024;
    gLexerState.currentToken = malloc(1024 * sizeof(char));

    _LexerReset();
}

void _LexerReset()
{
    gLexerState.currentTokenLength = 0;
    gLexerState.currentLine = 1;
    gLexerState.currentColumn = 1;
    gLexerState.currentTokenColumn = 1;
    gLexerState.readState = RS_NOTHING;
    gLexerState.stringEscape = 0;
    gLexerState.octalEscape = 0;
    gLexerState.hexEscape = 0;
}

void _LexerStoreCharacter(char newChar)
{
    if (gLexerState.currentTokenLength == gLexerState.currentTokenMaximum)
    {
        char *newBuffer = malloc(2 * gLexerState.currentTokenMaximum * sizeof(char));
        memcpy(newBuffer, gLexerState.currentToken, gLexerState.currentTokenMaximum * sizeof(char));
        free(gLexerState.currentToken);
        gLexerState.currentToken = newBuffer;
        gLexerState.currentTokenMaximum = 2 * gLexerState.currentTokenMaximum;
    }
    gLexerState.currentToken[gLexerState.currentTokenLength++] = newChar;
}

void _LexerFlushToken()
{
    // TODO: Store token result
    if (gLexerState.readState != RS_NOTHING)
    {
        _LexerStoreCharacter('\0');
        if (gLexerState.readState == RS_TEXT)
        {
            enum TokenType type = _LexerGetKeywordType(gLexerState.currentToken);
            printf("LEX %i: %s (KEYWORD %i)\n", gLexerState.readState, gLexerState.currentToken, type);
        }
        else
        {
            printf("LEX %i: %s\n", gLexerState.readState, gLexerState.currentToken);
        }
    }
    gLexerState.currentTokenLength = 0;
    gLexerState.readState = RS_NOTHING;
}

int _LexerWhiteSpace(char newChar)
{
    if (isspace(newChar))
    {
        return 1;
    }

    gLexerState.currentTokenColumn = gLexerState.currentColumn;
    if (isalpha(newChar)) gLexerState.readState = RS_TEXT;
    else if (isdigit(newChar)) gLexerState.readState = RS_NUMBER;
    else if (!isspace(newChar)) gLexerState.readState = RS_SYMBOLS;    
    return 0;
}

int _LexerText(char newChar)
{
    if (isalnum(newChar))
    {
        _LexerStoreCharacter(newChar);
        return 1;
    }
    else
    {
        _LexerFlushToken();
        gLexerState.readState = RS_NOTHING;
        return 0;
    }
}

int _LexerNumber(char newChar)
{
    // TODO: +/-, trailing characters, types, hex, etc
    if (isdigit(newChar))
    {
        _LexerStoreCharacter(newChar);
        return 1;
    }
    else
    {
        _LexerFlushToken();
        gLexerState.readState = RS_NOTHING;
        return 0;
    }
}

int _LexerSymbol(char newChar)
{
    if (!isspace(newChar) && !isalnum(newChar))
    {
        _LexerStoreCharacter(newChar);

        // TODO: Check for chains of symbols

        // Check for comments
        if (gLexerState.currentTokenLength == 2 && gLexerState.currentToken[0] == '/')
        {
            if (gLexerState.currentToken[1] == '/') gLexerState.readState = RS_COMMENTLINE;
            else if (gLexerState.currentToken[1] == '*') gLexerState.readState = RS_COMMENT;
        }
        // Check for strings
        if (gLexerState.currentTokenLength == 1 && gLexerState.currentToken[0] == '"')
        {
            // Clear the quotation marks
            gLexerState.currentTokenLength = 0;
            gLexerState.currentToken[0] = '\0';
            gLexerState.readState = RS_STRING;
        }
        // Check for preprocessor
        if (gLexerState.currentTokenLength == 1 && gLexerState.currentToken[0] == '#')
        {
            // Switch to preprocessor
            gLexerState.readState = RS_PREPROCESSOR;
        }
        return 1;
    }
    else
    {
        _LexerFlushToken();
        gLexerState.readState = RS_NOTHING;
        return 0;
    }
}

int _LexerComment(char newChar)
{
    _LexerStoreCharacter(newChar);

    if (gLexerState.currentTokenLength >= 4 &&
        gLexerState.currentToken[gLexerState.currentTokenLength - 2] == '*' &&
        gLexerState.currentToken[gLexerState.currentTokenLength - 1] == '/')
    {
        // Comment complete
        _LexerFlushToken();
        gLexerState.readState = RS_NOTHING;
    }

    return 1;
}

int _LexerCommentLine(char newChar)
{
    if (newChar == '\n')
    {
        // Comment complete
        _LexerFlushToken();
        gLexerState.readState = RS_NOTHING;
        return 0;
    }
    
    _LexerStoreCharacter(newChar);
    return 1;
}

int _LexerString(char newChar)
{
    if (gLexerState.hexEscape)
    {
        if (isdigit(newChar) || (newChar >= 'a' && newChar <= 'f') || (newChar >= 'A' && newChar <= 'F'))
        {
            gLexerState.hexEscapeValue *= 16;
            if (isdigit(newChar)) gLexerState.hexEscapeValue += newChar - '0';
            else if (newChar >= 'a' && newChar <= 'f') gLexerState.hexEscapeValue += newChar - 'a' + 10;
            else if (newChar >= 'A' && newChar <= 'F') gLexerState.hexEscapeValue += newChar - 'A' + 10;
        }
        else
        {
            // All done
            _LexerStoreCharacter(gLexerState.hexEscapeValue % 256);
            gLexerState.hexEscape = 0;
        }
    }
    else if (gLexerState.octalEscape)
    {
        if (newChar >= '0' && newChar <= '7')
        {
            gLexerState.octalEscapeValue *= 8;
            gLexerState.octalEscapeValue += newChar - '0';
        }
        else
        {
            // All done
            _LexerStoreCharacter(gLexerState.octalEscapeValue % 256);
            gLexerState.octalEscape = 0;
        }
    }

    if (gLexerState.hexEscape || gLexerState.octalEscape)
    {
        // Still capturing an escape sequence, do nothing.
    }
    else if (gLexerState.stringEscape)
    {
        gLexerState.stringEscape = 0;

        switch (newChar)
        {
        case('a') : _LexerStoreCharacter('\a'); break;
        case('b') : _LexerStoreCharacter('\b'); break;
        case('f') : _LexerStoreCharacter('\f'); break;
        case('n') : _LexerStoreCharacter('\n'); break;
        case('r') : _LexerStoreCharacter('\r'); break;
        case('t') : _LexerStoreCharacter('\t'); break;
        case('v') : _LexerStoreCharacter('\v'); break;
        case('\\') : _LexerStoreCharacter('\\'); break;
        case('\'') : _LexerStoreCharacter('\''); break;
        case('"') : _LexerStoreCharacter('\"'); break;
        case('?') : _LexerStoreCharacter('\?'); break;
        default: _LexerStoreCharacter(newChar); break;

        case('x') : gLexerState.hexEscape = 1; gLexerState.hexEscapeValue = 0; break;
        case('0') : gLexerState.octalEscape = 1; gLexerState.octalEscapeValue = 0; break;
        case('1') : gLexerState.octalEscape = 1; gLexerState.octalEscapeValue = 1; break;
        case('2') : gLexerState.octalEscape = 1; gLexerState.octalEscapeValue = 2; break;
        case('3') : gLexerState.octalEscape = 1; gLexerState.octalEscapeValue = 3; break;
        case('4') : gLexerState.octalEscape = 1; gLexerState.octalEscapeValue = 4; break;
        case('5') : gLexerState.octalEscape = 1; gLexerState.octalEscapeValue = 5; break;
        case('6') : gLexerState.octalEscape = 1; gLexerState.octalEscapeValue = 6; break;
        case('7') : gLexerState.octalEscape = 1; gLexerState.octalEscapeValue = 7; break;
        }
    }
    else if (newChar == '\\')
    {
        gLexerState.stringEscape = 1;
    }
    else if (newChar == '\"')
    {
        // String complete
        _LexerFlushToken();
        gLexerState.readState = RS_NOTHING;
    }
    else
    {
        _LexerStoreCharacter(newChar);
    }

    return 1;
}

int _LexerPreprocessor(char newChar)
{
    if (isspace(newChar))
    {
        // Command complete
        _LexerFlushToken();
        gLexerState.readState = RS_NOTHING;
        return 0;
    }

    _LexerStoreCharacter(newChar);
    return 1;
}

enum KeywordType _LexerGetKeywordType(char *text)
{
    for (int i = 0; i < KEYWORD_COUNT; i++)
    {
        if (0 == strcmp(text, gKeywords[i]))
        {
            return i;
        }
    }
    return KEYWORD_NOTHING;
}

void LexerTokenize()
{
    _LexerReset();

    char readChar = getchar();
    while (readChar != EOF)
    {
        // Processing
        int result = 0;
        while (result == 0)
        {
            switch (gLexerState.readState)
            {
            case(RS_NOTHING) :
                result = _LexerWhiteSpace(readChar);
                break;
            case(RS_TEXT) :
                result = _LexerText(readChar);
                break;
            case(RS_NUMBER) :
                result = _LexerNumber(readChar);
                break;
            case(RS_SYMBOLS) :
                result = _LexerSymbol(readChar);
                break;
            case(RS_COMMENT) :
                result = _LexerComment(readChar);
                break;
            case(RS_COMMENTLINE) :
                result = _LexerCommentLine(readChar);
                break;
            case(RS_STRING) :
                result = _LexerString(readChar);
                break;
            case(RS_PREPROCESSOR) :
                result = _LexerPreprocessor(readChar);
                break;
            default:
                result = 1;
            }
        }

        // Update counters
        gLexerState.currentColumn++;
        if (readChar == '\n')
        {
            gLexerState.currentColumn = 1;
            gLexerState.currentLine++;
        }
        readChar = getchar();
    }
    _LexerFlushToken();
}
